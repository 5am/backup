#!/usr/bin/env python3
##########################################################################
#  'Backup'
#  Copyright (C) 2020  Sam Howell
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Author: Sam Howell <mail AT samhowell DOT uk>
##########################################################################

version = 'v1.4.0'

import subprocess
import os
import sys
import pyfiglet
import time

home = os.environ['HOME']

# Print script title banner
banner = pyfiglet.figlet_format('Backup', font="slant")
print(banner)
print(version)

# Define user variables
sd_id = '<SD UUID>'
ssd_id = '<SSD UUID>'
vc_pass = '<VeraCrypt passphrase>'

# Define functions for device checks
def checkSD():
    process = subprocess.run(['sudo blkid | grep ' + sd_id], shell=True, capture_output=True, text=True)
    if process.returncode != 0:
        print('[Error] Device not found!')
        print()
        exit()
    else:
        print()
        print('Mounting and unlocking SD...')
        time.sleep(1)
        process = subprocess.run(['sudo mount UUID=' + sd_id + ' /media/$USER/SD\ Backup/ && sudo veracrypt -t --non-interactive /media/$USER/SD\ Backup/Backup -p ' + vc_pass + ' /media/veracrypt2/'], shell=True, capture_output=True, text=True)
        # Check device is mounted and unlocked
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
            print()

def checkSSD():
    process = subprocess.run(['sudo blkid | grep ' + ssd_id], shell=True, capture_output=True, text=True)
    if process.returncode != 0:
        print('[Error] Device not found!')
        print()
        exit()
    else:
        print()
        print('Mounting and unlocking SSD...')
        time.sleep(1)
        process = subprocess.run(['sudo mount UUID=' + ssd_id + ' /media/$USER/SSD/ && sudo veracrypt -t --non-interactive /media/$USER/SSD/VeraCrypt -p ' + vc_pass + ' /media/veracrypt1/'], shell=True, capture_output=True, text=True)
        # Check device is mounted and unlocked
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
            print()

def checkSDSSD():
    process1 = subprocess.run(['sudo blkid | grep ' + sd_id], shell=True, capture_output=True, text=True)
    process2 = subprocess.run(['sudo blkid | grep ' + ssd_id], shell=True, capture_output=True, text=True)
    if process1.returncode != 0:
        print('[Error] One or more devices not found!')
        print()
        exit()
    elif process2.returncode != 0:
        print('[Error] One or more devices not found!')
        print()
        exit()
    else:
        print()
        print('Mounting and unlocking SD & SSD...')
        time.sleep(1)
        process = subprocess.run(['sudo mount UUID=' + sd_id + ' /media/$USER/SD\ Backup/ && sudo mount UUID=' + ssd_id + ' /media/$USER/SSD/ && sudo veracrypt -t --non-interactive /media/$USER/SSD/VeraCrypt -p ' + vc_pass + ' /media/veracrypt1/ && sudo veracrypt -t --non-interactive /media/$USER/SD\ Backup/Backup -p ' + vc_pass + ' /media/veracrypt2/'], shell=True, capture_output=True, text=True)
        # Check devices are mounted and unlocked
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
            print()

# Define device type function and parameters
def deviceType(device):
    if (device == 1):
        device = 'SD'
        # Check device is connected
        checkSD()

    if (device == 2):
        device = 'SSD'
        # Check device is connected
        checkSSD()

    if (device == 3):
        device = 'SD & SSD'
        # Check devices are connected
        checkSDSSD()

# Define backup type function and parameters
def backupType(source, device):
    if (source == 1 and device == 1):
        source = home + '/'
        device = 'SD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(["sudo rsync -a --update --delete --include-from '/home/$USER/Documents/Development/Python/Local/Backup/include-SD.txt' --exclude-from '/home/$USER/Documents/Development/Python/Local/Backup/exclude-SD.txt' /home/$USER /media/veracrypt2/Backup/"], shell=True, capture_output=True, text=True)
        # Check success of backup
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount device
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + sd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 1 and device == 2):
        source = home + '/'
        device = 'SSD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(["sudo rsync -a --update --delete --include-from '/home/$USER/Documents/Development/Python/Local/Backup/include-SSD.txt' --exclude-from '/home/$USER/Documents/Development/Python/Local/Backup/exclude-SSD.txt' /home/$USER /media/veracrypt1/Backup/"], shell=True, capture_output=True, text=True)
        # Check success of backup
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount device
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + ssd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 1 and device == 3):
        source = home + '/'
        device = 'SD & SSD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(["sudo rsync -a --update --delete --include-from '/home/$USER/Documents/Development/Python/Local/Backup/include-SD.txt' --exclude-from '/home/$USER/Documents/Development/Python/Local/Backup/exclude-SD.txt' /home/$USER /media/veracrypt2/Backup/ && sudo rsync -a --update --delete --include-from '/home/$USER/Documents/Development/Python/Local/Backup/include-SSD.txt' --exclude-from '/home/$USER/Documents/Development/Python/Local/Backup/exclude-SSD.txt' /home/$USER /media/veracrypt1/Backup/"], shell=True, capture_output=True, text=True)
        # Check success of backups
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount devices
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + sd_id + ' UUID=' + ssd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 2 and device == 1):
        source = '~/Pictures/'
        device = 'SD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Pictures/ /media/veracrypt2/Backup/$USER/Pictures/'], shell=True, capture_output=True, text=True)
        # Check success of backup
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount device
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + sd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 2 and device == 2):
        source = '~/Pictures/'
        device = 'SSD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Pictures/ /media/veracrypt1/Backup/$USER/Pictures/'], shell=True, capture_output=True, text=True)
        # Check success of backup
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount device
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + ssd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 2 and device == 3):
        source = '~/Pictures/'
        device = 'SD & SSD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Pictures/ /media/veracrypt2/Backup/$USER/Pictures/ && sudo rsync -a --update --delete /home/$USER/Pictures/ /media/veracrypt1/Backup/$USER/Pictures/'], shell=True, capture_output=True, text=True)
        # Check success of backups
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount devices
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + sd_id + ' UUID=' + ssd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 3 and device == 1):
        source = '~/Documents/Development/'
        device = 'SD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Documents/Development/ /media/veracrypt2/Backup/$USER/Documents/Development/'], shell=True, capture_output=True, text=True)
        # Check success of backup
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount device
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + sd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 3 and device == 2):
        source = '~/Documents/Development/'
        device = 'SSD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Documents/Development/ /media/veracrypt1/Backup/$USER/Documents/Development/'], shell=True, capture_output=True, text=True)
        # Check success of backup
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount device
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + ssd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 3 and device == 3):
        source = '~/Documents/Development/'
        device = 'SD & SSD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Documents/Development/ /media/veracrypt2/Backup/$USER/Documents/Development/ && sudo rsync -a --update --delete /home/$USER/Documents/Development/ /media/veracrypt1/Backup/$USER/Documents/Development/'], shell=True, capture_output=True, text=True)
        # Check success of backups
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount devices
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + sd_id + ' UUID=' + ssd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 4 and device == 1):
        source = '~/Documents/Web Development/'
        device = 'SD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Documents/Web\ Development/ /media/veracrypt2/Backup/$USER/Documents/Web\ Development/'], shell=True, capture_output=True, text=True)
        # Check success of backup
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount device
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + sd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 4 and device == 2):
        source = '~/Documents/Web Development/'
        device = 'SSD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Documents/Web\ Development/ /media/veracrypt1/Backup/$USER/Documents/Web\ Development/'], shell=True, capture_output=True, text=True)
        # Check success of backup
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount device
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + ssd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 4 and device == 3):
        source = '~/Documents/Web Development/'
        device = 'SD & SSD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Documents/Web\ Development/ /media/veracrypt2/Backup/$USER/Documents/Web\ Development/ && sudo rsync -a --update --delete /home/$USER/Documents/Web\ Development/ /media/veracrypt1/Backup/$USER/Documents/Web\ Development/'], shell=True, capture_output=True, text=True)
        # Check success of backups
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount devices
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + sd_id + ' UUID=' + ssd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 5 and device == 1):
        source = '~/Documents/Writing/'
        device = 'SD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Documents/Writing/ /media/veracrypt2/Backup/$USER/Documents/Writing/'], shell=True, capture_output=True, text=True)
        # Check success of backup
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount device
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + sd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 5 and device == 2):
        source = '~/Documents/Writing/'
        device = 'SSD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Documents/Writing/ /media/veracrypt1/Backup/$USER/Documents/Writing/'], shell=True, capture_output=True, text=True)
        # Check success of backup
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount device
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + ssd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 5 and device == 3):
        source = '~/Documents/Writing/'
        device = 'SD & SSD'
        # Back up to device
        print('Backing up ' + source + ' to ' + device + '...')
        time.sleep(1)
        process = subprocess.run(['sudo rsync -a --update --delete /home/$USER/Documents/Writing/ /media/veracrypt2/Backup/$USER/Documents/Writing/ && sudo rsync -a --update --delete /home/$USER/Documents/Writing/ /media/veracrypt1/Backup/$USER/Documents/Writing/'], shell=True, capture_output=True, text=True)
        # Check success of backups
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            time.sleep(1)
        # Unmount devices
        print()
        print('Unmounting...')
        process = subprocess.run(['sudo veracrypt -d && sudo umount UUID=' + sd_id + ' UUID=' + ssd_id], shell=True, capture_output=True, text=True)
        if process.returncode != 0:
            print('[Error]', process.stderr)
            exit()
        else:
            print('...OK')
            print()
            exit()

    if (source == 6 and device == 4):
        source = home + '/Pictures/'
        device = 'Nextcloud'
        print()
        print('Backing up ' + source + ' to Nextcloud...')
        time.sleep(1)
        print()
        subprocess.run(['rclone sync -P ~/Pictures/ encrypted:/Pictures/'], shell=True, capture_output=False, text=True)
        print()
        exit()

    if (source == 7 and device == 4):
        source = home + '/Documents/'
        device = 'Nextcloud'
        print()
        print('Backing up ' + source + ' to Nextcloud...')
        time.sleep(1)
        print()
        subprocess.run(['rclone sync -P --exclude "Archive" --exclude "Assets" --exclude "Lynda" ~/Documents/ encrypted:/Documents/'], shell=True, capture_output=False, text=True)
        print()
        exit()

    if (source == 8 and device == 4):
        source = home + '/Documents/Development/'
        device = 'Nextcloud'
        print()
        print('Backing up ' + source + ' to Nextcloud...')
        time.sleep(1)
        print()
        subprocess.run(['rclone sync -P --exclude "Archive" --exclude "Assets" --exclude "Lynda" ~/Documents/Development/ encrypted:/Documents/Development/'], shell=True, capture_output=False, text=True)
        print()
        exit()

    if (source == 9 and device == 4):
        source = home + '/Documents/Web Development/'
        device = 'Nextcloud'
        print('Backing up ' + source + ' to Nextcloud...')
        time.sleep(1)
        print()
        subprocess.run(['rclone sync -P ~/Documents/Web\ Development/ encrypted:/Documents/Web\ Development/'], shell=True, capture_output=False, text=True)
        print()
        exit()

    if (source == 10 and device == 4):
        source = home + '/Documents/Writing/'
        device = 'Nextcloud'
        print()
        print('Backing up ' + source + ' to Nextcloud...')
        time.sleep(1)
        print()
        subprocess.run(['rclone sync -P ~/Documents/Writing/ encrypted:/Documents/Writing/'], shell=True, capture_output=False, text=True)
        print()
        exit()

menus = """
    ___ Devices/locations ______________
    ************************************
        Local
                SD
                SSD
                SD/SSD
        Remote
                Nextcloud (nc)

    ___ Backup types ___________________
    ************************************
        Local
                1. ~/
                2. ~/Pictures/
                3. ~/Documents/Development/
                4. ~/Documents/Web Development/
                5. ~/Documents/Writing/
        Remote
                6. ~/Pictures/
                7. ~/Documents/
                8. ~/Documents/Development/
                9. ~/Documents/Web Development/
               10. ~/Documents/Writing/
    ____________________________________

    """

# Define valid user inputs
valid_devices = ['sd', 'SD', 'ssd', 'SSD', 'sd/ssd', 'SD/SSD', 'nc', 'NC', 'nextcloud', 'Nextcloud', 'NEXTCLOUD']
valid_nextcloud = ['nc', 'NC', 'nextcloud', 'Nextcloud', 'NEXTCLOUD']
valid_SD = ['sd', 'SD']
valid_SSD = ['ssd', 'SSD']
valid_sources = list(map(str,range(1,11)))

print(menus)
device = input('Device(s)/location: ')
source = input('Source: ')

while device not in valid_devices or source not in valid_sources:
    print()
    print('Valid options not chosen. Try again.')
    print()
    device = input('Device(s)/location: ')
    source = input('Source: ')

def selection():
    if device in valid_nextcloud:
        print()
        print('You chose location: Nextcloud, source: ' + source)
    else:
        print()
        print('You chose device(s)/location: ' + device.upper() + ', source: ' + source)

selection()
confirmation = input('Confirm [Y/y] ')

if confirmation in ['y', 'Y']:
    if source == '1' and device in valid_SD:
        deviceType(1)
        backupType(1, 1)
    elif source == '2' and device in valid_SD:
        deviceType(1)
        backupType(2, 1)
    elif source == '3' and device in valid_SD:
        deviceType(1)
        backupType(3, 1)
    elif source == '4' and device in valid_SD:
        deviceType(1)
        backupType(4, 1)
    elif source == '5' and device in valid_SD:
        deviceType(1)
        backupType(5, 1)
    elif source == '1' and device in valid_SSD:
        deviceType(2)
        backupType(1, 2)
    elif source == '2' and device in valid_SSD:
        deviceType(2)
        backupType(2, 2)
    elif source == '3' and device in valid_SSD:
        deviceType(2)
        backupType(3, 2)
    elif source == '4' and device in valid_SSD:
        deviceType(2)
        backupType(4, 2)
    elif source == '5' and device in valid_SSD:
        deviceType(2)
        backupType(5, 2)
    elif source == '1' and device in valid_devices:
        deviceType(3)
        backupType(1, 3)
    elif source == '2' and device in valid_devices:
        deviceType(3)
        backupType(2, 3)
    elif source == '3' and device in valid_devices:
        deviceType(3)
        backupType(3, 3)
    elif source == '4' and device in valid_devices:
        deviceType(3)
        backupType(4, 3)
    elif source == '5' and device in valid_devices:
        deviceType(3)
        backupType(5, 3)
    elif source == '6' and device in valid_nextcloud:
        backupType(6, 4)
    elif source == '7' and device in valid_nextcloud:
        backupType(7, 4)
    elif source == '8' and device in valid_nextcloud:
        backupType(8, 4)
    elif source == '9' and device in valid_nextcloud:
        backupType(9, 4)
    elif source == '10' and device in valid_nextcloud:
        backupType(10, 4)
    elif source in list(map(str,range(1,8))) and device in valid_nextcloud:
        print()
        print('Valid source-remote backup not chosen.')
        print()
        exit()
    else:
        print('Something went wrong!')
else:
    print()
    exit()
