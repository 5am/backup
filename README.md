# Description

'Backup' allows you to backup local files and directories to a number of predefined locally connected devices or remote locations. The code included in this repo is mostly based on my own local version of the program, hence the unique device IDs, directory names etc.

Before running, inspect the code. A number of lines require completion with user-specific information (passphrases, directory paths etc.) in order for the program to be of any use. I've indicated with comments where most of these changes need to be made. The following variables should be replaced:
<ul>
<li>UUIDs
<li>PASSPHRASE
</ul>

![alt text](https://gitlab.com/5am/backup/raw/master/backup_screenshot.jpg "Backup screenshot")

# Dependencies

'Backup' was initially written for personal use and assumes the following (non/)standard software is installed on a GNU/Linux system:
<ul>
<li>Python 3
<li>Rsync
<li>Rclone
<li>VeraCrypt
<li>python3-pyfiglet
</ul>

# Verify

Move to the directory with the downloaded files (e.g. Downloads):
```
cd /home/$USER/Downloads
```

Fetch my public key:
```
wget https://samhowell.uk/pgp/publickey.mail@samhowell.uk.asc
```

Import the key into GPG:
```
gpg --import publickey.mail@samhowell.uk.asc
```

Use GPG to verify the signature against the key:
```
gpg --verify backup.py.sig backup.py
```

Check the integrity of the file:
```
sha256sum -c backup.py.SHA256
```

You should see the following output:
```
backup.py: OK
```
